# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import configuration
from . import reports
from . import dash


def register():
    Pool.register(
        reports.DashReport,
        dash.AccessReport,
        dash.DashAccess,
        dash.AccessApp,
        dash.DashApp,
        configuration.Configuration,
        module='dash', type_='model')
