# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields


class DashReport(ModelSQL, ModelView):
    'Dashboard Report'
    __name__ = 'dash.report'
    name = fields.Char('Name', required=True, select=True)
    model = fields.Char('Model', select=True)
    method = fields.Char('Method', select=True)
    type = fields.Selection([
        ('bar', 'Bar'),
        ('line', 'Line'),
        ('pie', 'Pie'),
        ('doughnut', 'Doughnut'),
        ('card_info', 'Card Info'),
        ('bar_line', 'Bar-Line'),
        ('table', 'Table'),
    ], 'Type', required=True)
    comment = fields.Char('Comment')
    colspan = fields.Integer('Cols. Span', help="Number of columns to take")
    in_thousands = fields.Boolean('In Thousands')
