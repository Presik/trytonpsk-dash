# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from trytond.model import ModelView, ModelSQL, fields

KIND = [
    ('internal', 'Internal'),
    ('external', 'External'),
]


class DashAppBase(ModelSQL, ModelView):
    'Dash App Base'
    __name__ = 'dash.app.base'
    company = fields.Many2One('company.company', 'Company', required=True)
    icon = fields.Selection([
        ('key', 'key'),
        ('motorcycle', 'motorcycle'),
        ('shipping fast', 'shipping fast'),
        ('shopping bag', 'shopping bag'),
        ('clipboard check', 'clipboard check'),
        ('shield', 'shield'),
        ('dolly', 'dolly'),
        ('th', 'th'),
        ('cart', 'cart'),
        ('utensils', 'utensils'),
        ('coffee', 'coffee'),
        ('tasks', 'tasks'),
        (None, ''),
        ], 'Icon')
    kind = fields.Selection(KIND, 'Kind')

    @staticmethod
    def default_kind():
        return 'internal'


class DashAccess(ModelSQL, ModelView):
    'Dash Access'
    __name__ = 'dash.access'
    user = fields.Many2One('res.user', 'User', required=True, select=True)
    reports = fields.One2Many('dash.access.report', 'dash_access', 'Reports')
    apps = fields.Many2Many('dash.access-dash.app', 'dash_access', 'dash_app',
        'Apps')


class AccessReport(ModelSQL, ModelView):
    'Dash Access - Report'
    __name__ = 'dash.access.report'
    sequence = fields.Integer('Sequence', required=True)
    report = fields.Many2One('dash.report', 'Dash Report', required=True)
    dash_access = fields.Many2One('dash.access', 'Access', required=True,
        ondelete='CASCADE')
    category = fields.Selection([
        ('', ''),
        ('accounting', 'Accounting'),
        ('sales', 'Sales'),
        ('purchases', 'Purchases'),
        ('manager', 'Manager'),
        ('stock', 'Stock'),
        ('production', 'Production'),
        ('human_resources', 'HR'),
        ('payroll', 'Payroll'),
        ('crm', 'CRM'),
        ('hotel', 'Hotel'),
    ], 'Category', required=False)
    color = fields.Selection([
        ('', ''),
        ('antiquewhite', 'antiquewhite'),
    ], 'Color')


class AccessApp(ModelSQL):
    'Dash Access - App'
    __name__ = 'dash.access-dash.app'
    _table = 'dash_access_dash_app_rel'
    dash_access = fields.Many2One('dash.access', 'Access',
        ondelete='CASCADE', select=True, required=True)
    dash_app = fields.Many2One('dash.app', 'Dash App',
        ondelete='RESTRICT', select=True, required=True)


class DashApp(ModelSQL, ModelView):
    'Dash App'
    __name__ = 'dash.app'
    name = fields.Char('Name', required=True)
    origin = fields.Reference('Origin', selection='get_origin')
    active = fields.Boolean('Active')
    app_name = fields.Selection('get_selection', 'App Name')
    icon = fields.Function(fields.Char('Icon'), 'get_icon')
    kind = fields.Function(fields.Selection(KIND, 'Kind'), 'get_kind')
    additional_path = fields.Function(fields.Char('Additional Path'), 'get_additional_path')

    @staticmethod
    def default_active():
        return True

    @classmethod
    def _get_origin(cls):
        'Return list of Model names for origin Reference'
        return []

    @classmethod
    def get_origin(cls):
        Model = Pool().get('ir.model')
        models = cls._get_origin()
        models = Model.search([
            ('model', 'in', models),
        ])
        return [('', '')] + [(m.model, m.name) for m in models]

    def get_icon(self, name=None):
        if self.origin and hasattr(self.origin, 'icon'):
            return self.origin.icon
        return 'file'

    def get_kind(self, name=None):
        if self.origin and hasattr(self.origin, 'kind'):
            return self.origin.kind
        return 'internal'

    def get_additional_path(self, name=None):
        if self.origin and hasattr(self.origin, 'additional_path'):
            return self.origin.additional_path

    @classmethod
    def get_selection(cls):
        return []
