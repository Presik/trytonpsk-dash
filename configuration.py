# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval, If, In, Get
from trytond.transaction import Transaction


class Configuration(ModelSQL, ModelView):
    'Dash Configuration'
    __name__ = 'dash.configuration'
    company = fields.Many2One('company.company', 'Company', required=True,
        domain=[
            ('id', If(In('company',
            Eval('context', {})), '=', '!='), Get(Eval('context', {}),
            'company', 0))
        ])
    logo_company_link = fields.Char('Logo Company Link',)

    @staticmethod
    def default_company():
        return Transaction().context.get('company') or None
